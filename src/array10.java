import java.util.Scanner;
import javax.swing.text.html.CSS;

public class array10 {
    public static void main(String[] args) {
        int size;
        int arr[];
        int uniqearr[];
        int unisize=0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of Element: ");
        size = sc.nextInt();
        arr = new int[size];
        uniqearr = new int[size];
        for (int i=0;i<arr.length;i++) {
            System.out.print("Element " + i + ": ");
            int temp = sc.nextInt();
            int index = -1;
            for (int j=0; j<unisize;j++) {
                if (arr[j] == temp) {
                    index = j;
                }
            }
            if (index<0) {
                arr[unisize] = temp;
                unisize++;
            }
        }
        System.out.print("arr = ");
        for (int i = 0; i < unisize; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
