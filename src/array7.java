import java.util.Scanner;

public class array7 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        Scanner sc = new Scanner(System.in);
        System.out.print("please input arr[0]: ");
        arr[0] = sc.nextInt();
        System.out.print("please input arr[1]: ");
        arr[1] = sc.nextInt();
        System.out.print("please input arr[3]: ");
        arr[2] = sc.nextInt();
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        System.out.println("sum = " + sum);

        double avg = ((double)sum)/arr.length;
        System.out.println("avg = " + avg);

        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (min>arr[i]) {
                min = arr[i];
            }
        }
        System.out.println("min = " + min);
    }
}

