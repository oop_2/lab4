import java.util.Scanner;

public class array9 {
    
    public static void main(String[] args) {
        int arr[] = new int[3];
        Scanner sc = new Scanner(System.in);
        System.out.print("please input arr[0]: ");
        arr[0] = sc.nextInt();
        System.out.print("please input arr[1]: ");
        arr[1] = sc.nextInt();
        System.out.print("please input arr[3]: ");
        arr[2] = sc.nextInt();
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        System.out.print("Please input search value:");
        int searchvalue = sc.nextInt();
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == searchvalue) {
                index=i;
                break;
            }
        }
        if (index>=0) {
            System.out.println("found at index: " + index);
        } else {
            System.out.println("not found");
        }
    }
}

