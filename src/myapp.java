import java.util.Scanner;

public class myapp {
    static void printwelcome() {
        System.out.println("Welcome to my app!!!");
    }

    static void printmenu() {
        System.out.println("--Menu--");
        System.out.println("1.Print Hello World N time");
        System.out.println("2.Add 2 number");
        System.out.println("3.Exit");
    }

    static int inputchoice() {
        int choice;
        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please input your choice(1-3): ");
            choice = sc.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error: Please input between 1-3");
        }
    }

    public static void main(String[] args) {
        int choice = 0;
        printwelcome();
        while (true) {
            printmenu();
            choice = inputchoice();
            switch (choice) {
                case 1:
                    printhelloworld();
                    break;
                case 2:
                    addnumber();
                    break;
                case 3:
                    printbye();
                    break;
            }
        }
    }

    static void addnumber() {
        Scanner sc = new Scanner(System.in);
        int first;
        int second;
        int sum = 0;
        System.out.print("Please input first number: ");
        first = sc.nextInt();
        System.out.print("Please input second number: ");
        second = sc.nextInt();
        sum = first + second;
        System.out.println("Result = " + sum);
    }

    static void printhelloworld() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = sc.nextInt();
        for (int i = 0; i < time; i++) {
            System.out.println("HelloWorld!!!");
        }
    }

    static void printbye() {
        System.out.println("Bye!!!");
        System.exit(0);
    }
}
